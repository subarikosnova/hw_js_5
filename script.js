class Card {
    constructor(post, getPostAuthorInfo) {
        this.post = post;
        this.getPostAuthorInfo = getPostAuthorInfo;
        this.element = this.createCardElement();
        this.attachEventListeners();
    }

    createCardElement() {
        const cardElement = document.createElement("div");
        cardElement.classList.add("card");

        cardElement.innerHTML = `
            <div class="card-wrapper">
            <div class="img">
            <img src="micon.jpg" alt="ava">
            <h2 class="card-title">${this.post.title}</h2>
            </div>
            
            
            <p class="title-text">${this.post.body}</p>
            
            
            <div class="img">
            <img src="avatar.png" alt="ava">
            <p class="author">Author: ${this.getPostAuthorInfo(this.post.userId)}</p>
            </div>
            
            <button class="delete-button" data-post-id="${this.post.id}"><img src="delete2.png" alt="del">Delete</button>
            </div>
        `;

        return cardElement;
    }

    attachEventListeners() {
        const deleteButton = this.element.querySelector(".delete-button");
        deleteButton.addEventListener("click", () => this.handleDeleteButtonClick());
    }

    handleDeleteButtonClick() {
        fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, {
            method: "DELETE",
        })
            .then(response => {
                if (response.ok) {
                    this.element.remove();
                } else {
                    console.error("Failed to delete post. Server response not OK.");
                }
            })
            .catch(error => console.error("Error while deleting post:", error));
    }
}

document.addEventListener("DOMContentLoaded", function () {
    let usersData; // Declare usersData in the outer scope

    // Отримання списку користувачів
    fetch("https://ajax.test-danit.com/api/json/users")
        .then(response => response.json())
        .then(data => {
            usersData = data; // Assign data to usersData
            console.log("Список користувачів:", usersData);

            // Отримання списку публікацій
            return fetch("https://ajax.test-danit.com/api/json/posts");
        })
        .then(response => response.json())
        .then(postsData => {
            console.log("Список публікацій:", postsData);

            // Відобразити публікації на сторінці
            displayPosts(postsData);
        })
        .catch(error => console.error("Помилка:", error));

    // Функція для відображення публікацій на сторінці
    function displayPosts(posts) {
        // Отримати елемент, в який будемо вставляти публікації
        const postsContainer = document.getElementById("posts-container");

        // Створити екземпляри класу Card для кожної публікації і додати їх до контейнера
        posts.forEach(post => {
            const card = new Card(post, getPostAuthorInfo);
            postsContainer.appendChild(card.element);
        });
    }

    // Функція для отримання інформації про автора публікації
    function getPostAuthorInfo(userId) {
        const user = usersData.find(user => user.id === userId);
        if (user) {
            return `${user.name} ${user.surname}, Email: ${user.email}`;
        }
        return "Author not found";
    }
});
